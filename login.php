<?php

require_once 'vendor/autoload.php';
use Services\Blog;
use Services\TwigLoader;

session_start();
if (!empty($_SESSION['login'])) {
    header("Location:index.twig");
}
if (Blog::checkPostArrayElements(['login', 'password'])) {
    $login = new Blog();
    if ($login->getUserByLoginAndPassword(
        $_POST['login'],
        $_POST['password']
    )) {
        $_SESSION['uuid'] = $login->getUserUuidByLoginAndPassword($_POST['login'], $_POST['password']);
        $_SESSION['login'] = $_POST['login'];
        header('Location:index.php');
    }
}

$twig = new TwigLoader();
$twig = $twig->getEnvironment();
echo $twig->render('index.twig', ['css' => 'signin.css','title' => 'Login','page' => 'login.twig']);


