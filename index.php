<?php
session_start();
require_once 'vendor/autoload.php';
use Services\Blog;
use Services\TwigLoader;

if (isset($_GET['logout'])) {
    session_destroy();
    header("Location:login.php");
}

$twig = new TwigLoader();
$posts = new Blog();

$twig = $twig->getEnvironment();
$query = $posts->getPosts();
$twig->addGlobal("session",$_SESSION);
echo $twig->render('index.twig', ['css' => 'blog.css','title' => 'Blog','posts' => $query, 'page' => 'posts-list.twig']);



