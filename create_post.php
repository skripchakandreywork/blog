<?php
session_start();
require_once 'vendor/autoload.php';
use Services\Blog;
use Services\TwigLoader;
if (Blog::checkPostArrayElements(['login', 'uuid'])) {
    header("Location:index.twig");
}

$post = new Blog();
if (Blog::checkPostArrayElements(['title', 'desk'])) {
    if ($post->getUserIdByLoginAndUuid($_SESSION['login'], $_SESSION['uuid'])) {
        if ($post->createPost($_SESSION['login'], $_POST['title'], $_POST['desk'])) {
            header('Location:index.php');
        }
    }
}

$twig = new TwigLoader();

$twig = $twig->getEnvironment();
echo $twig->render('index.twig', ['css' => 'signin.css','title' => 'Create Post', 'page' => 'create-post.twig']);




