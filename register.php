<?php
session_start();
if (!empty($_SESSION['login'])) {
    header("Location:index.twig");
}
require_once 'vendor/autoload.php';

use Services\Blog;
use Services\TwigLoader;

if (Blog::checkPostArrayElements(['login', 'password', 'email', 'name', 'surname', 'birthday'])) {
    $reg = new Blog();
    if (!$reg->CheckingUniquenessLoginAndEmail($_POST['login'], $_POST['email'])) {
        if ($reg->addUser(
            $_POST['login'],
            $_POST['password'],
            $_POST['email'],
            $_POST['name'],
            $_POST['surname'],
            $_POST['birthday'],
            $uuid = $reg->generateUuid()
        )) {
            $_SESSION['uuid'] = $uuid;
            $_SESSION['login'] = $_POST['login'];
            header('Location:index.twig');
        }
    }
}

$twig = new TwigLoader();
$twig = $twig->getEnvironment();
echo $twig->render('index.twig', ['css' => 'signin.css','title' => 'Sign Up','page' => 'register.twig']);
