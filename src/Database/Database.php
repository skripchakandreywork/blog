<?php
namespace Database;

use PDO;

class Database
{

    private const DB_USERNAME = 'root';
    private const DB_HOST = 'localhost;';
    private const DB_PASSWORD = 'root';
    private const DB_NAME = 'blog';


    public function connect():PDO
    {
        return new PDO("mysql:host=".self::DB_HOST."dbname=".self::DB_NAME, self::DB_USERNAME, self::DB_PASSWORD);
    }
}