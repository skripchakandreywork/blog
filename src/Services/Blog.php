<?php
namespace Services;

require_once 'vendor/autoload.php';
use PDO;
use Database\Database;

class Blog
{
    private $pdo;

    public function __construct()
    {
        $db = new Database();
        $this->pdo = $db->connect();
    }

    public function getUserByLoginAndPassword(string $login, string $password): bool
    {
        $query = $this->pdo->prepare("SELECT hash FROM users WHERE (login = :login)");
        $query->execute([':login' => $login]);
        $hash = $query->fetch(PDO::FETCH_COLUMN);
        return password_verify($password, $hash);
    }

    public function addUser(string $login, string $password, string $email, string $name, string $surname, string $birthday, string $uuid): bool
    {
        $prepare = $this->pdo->prepare("INSERT INTO users (login, hash, email, name, surname, birthday, uuid) VALUES (:login, :password, :email, :name, :surname, :birthday, :uuid)");
        return $prepare->execute([':login' => $login, ':password' => password_hash($password, PASSWORD_DEFAULT), ':email' => $email, ':name' => $name, ':surname' => $surname, ':birthday' => $birthday, ':uuid' => $uuid]);
    }

    public function getPosts(): array
    {
        $query = $this->pdo->prepare("SELECT * FROM posts");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createPost(string $user, string $title, string $desk): bool
    {
        $prepare = $this->pdo->prepare("INSERT INTO posts (user, title, text, date) VALUES (:user, :title, :desk, now())");
        return $prepare->execute([':user' => $user, ':title' => $title, ':desk' => $desk]);
    }

    public function generateUuid(): string
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            mt_rand(0, 0xffff),

            mt_rand(0, 0x0fff) | 0x4000,

            mt_rand(0, 0x3fff) | 0x8000,

            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function getUserUuidByLoginAndPassword(string $login, string $password): string
    {
        $prepare = $this->pdo->prepare("SELECT hash,uuid FROM users WHERE (login=:login)");
        $prepare->execute([':login' => $login]);
        $query = $prepare->fetch(PDO::FETCH_ASSOC);
        if (password_verify($password,$query['hash'])){
            return $query['uuid'];
        }
        return false;
    }

    public function getUserIdByLoginAndUuid(string $login, string $uuid): string
    {
        $query = $this->pdo->prepare("SELECT id FROM users WHERE (login=:login) AND (uuid=:uuid)");
        $query->execute([':login' => $login, ':uuid' => $uuid]);
        return $query->fetch(PDO::FETCH_COLUMN);
    }


    public function CheckingUniquenessLoginAndEmail(string $login, string $email): bool
    {
        $query = $this->pdo->prepare("SELECT login,email FROM users WHERE (login = :login) OR (email = :email)");
        $query->execute([':login' => $login, ':email' => $email]);
        return (bool)$query->fetch();
    }

    public static function checkPostArrayElements(array $keys):bool
    {
        foreach ($keys as $key) {
            if (isset($_SESSION[$key])){
                return false;
            }
            if (!isset($_POST[$key]) || empty($_POST[$key])) {
                return false;
            }
        }
        return true;
    }


}

