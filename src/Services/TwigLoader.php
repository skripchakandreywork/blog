<?php

namespace Services;

require_once 'vendor/autoload.php';
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigLoader
{
    public function getEnvironment():object
    {
        $loader = new FilesystemLoader('templates');
        return new Environment($loader);
    }
}